package com.ks.tools;

import junit.framework.TestCase;

public class Encrypt_3desTest extends TestCase
{
    public void testEncrypt() throws Exception
    {
        String VLstrResultado, VLstrTarjeta, VLstrLlave;

        try
        {
            VLstrTarjeta = "5413330002001171";
            VLstrLlave = "10AB93B59352E946B0844FC22ED1DE34";
            Encrypt_3des VLobjEnc = new Encrypt_3des();
            VLstrResultado = VLobjEnc.encrypt(VLstrTarjeta, VLstrLlave);
            System.out.println("Tarjeta: " + VLstrTarjeta);
            System.out.println("Llave: " + VLstrLlave);
            System.out.println("Cifrado: " + VLstrResultado + "\n\n");

            VLstrTarjeta = "5413330002001171";
            VLstrLlave = "9B12B5555BE0B56047A173B432672310";
            VLobjEnc = new Encrypt_3des();
            VLstrResultado = VLobjEnc.encrypt(VLstrTarjeta, VLstrLlave);
            System.out.println("Tarjeta: " + VLstrTarjeta);
            System.out.println("Llave: " + VLstrLlave);
            System.out.println("Cifrado: " + VLstrResultado + "\n\n");

            VLstrTarjeta = "5413330002001171";
            VLstrLlave = "ACD4D3C82DA384A836733BA17A227CE4";
            VLobjEnc = new Encrypt_3des();
            VLstrResultado = VLobjEnc.encrypt(VLstrTarjeta, VLstrLlave);
            System.out.println("Tarjeta: " + VLstrTarjeta);
            System.out.println("Llave: " + VLstrLlave);
            System.out.println("Cifrado: " + VLstrResultado + "\n\n");

            VLstrTarjeta = "5413330002001171";
            VLstrLlave = "BB0205C0570C43117DFEB849F2AB2189";
            VLobjEnc = new Encrypt_3des();
            VLstrResultado = VLobjEnc.encrypt(VLstrTarjeta, VLstrLlave);
            System.out.println("Tarjeta: " + VLstrTarjeta);
            System.out.println("Llave: " + VLstrLlave);
            System.out.println("Cifrado: " + VLstrResultado + "\n\n");

            VLstrTarjeta = "5413330002001171";
            VLstrLlave = "3EAEE738A50BFC95A25F6BF65B8AF42F";
            VLobjEnc = new Encrypt_3des();
            VLstrResultado = VLobjEnc.encrypt(VLstrTarjeta, VLstrLlave);
            System.out.println("Tarjeta: " + VLstrTarjeta);
            System.out.println("Llave: " + VLstrLlave);
            System.out.println("Cifrado: " + VLstrResultado + "\n\n");


        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage());
        }

    }

}